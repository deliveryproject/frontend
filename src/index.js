import "babel-polyfill";
import React from "react";
import "./index.css";
import { render } from "react-dom";
import Root from "./components/Root";
import configureStore from "./store/configureStore";
import registerServiceWorker from "./registerServiceWorker";

const store = configureStore();

render(<Root store={store} />, document.getElementById("root"));
registerServiceWorker();
