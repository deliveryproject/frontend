import React, { Component } from "react";
import "./App.css";
import AllOrders from "./AllOrders";
import Signup from "./Signup";
import { Route, withRouter, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { getIsAuthenticated, getUser } from "../reducers/authentication";

import * as actions from "../actions/user";
import { Layout, notification } from "antd";
import LoadingIndicator from "./common/LoadingIndicator";
import AppHeader from "./common/AppHeader";
import Login from "./common/Login";
import Logout from "./common/Logout";

const { Content } = Layout;

class App extends Component {
  constructor(props) {
    super(props);

    notification.config({
      placement: "topRight",
      top: 70,
      duration: 3
    });
  }

  onLoginSuccess = () => {
    notification.success({
      message: "Delivery Project",
      description: "You're successfully logged in."
    });
  };

  onLoginFailed = () => {
    notification.error({
      message: "Delivery Project",
      description: "Your Username or Password is incorrect. Please try again!"
    });
  };

  onSignupSuccess = () => {
    notification.success({
      message: "Delivery Project",
      description: "You're successfully signed up."
    });
  };

  onSignupFailed = () => {
    notification.error({
      message: "Delivery Project",
      description: "Signup failed."
    });
  };

  render() {
    const { isAuthenticated, user, handleLogout } = this.props;
    return (
      <Layout className="app-container">
        <AppHeader
          isAuthenticated={isAuthenticated}
          user={user}
          handleLogout={() => {
            handleLogout();
            this.props.history.push("/");
          }}
        />
        <Content className="app-content">
          <div className="container">
            <Switch>
              <Route exact path="/" render={() => <AllOrders />} />
              <Route
                exact
                path="/login"
                render={() => (
                  <Login
                    onLoginSuccess={this.onLoginSuccess}
                    onLoginFailed={this.onLoginFailed}
                  />
                )}
              />
              <Route
                exact
                path="/logout"
                render={() => (
                  <Logout
                    handleLogout={() => {
                      handleLogout();
                    }}
                  />
                )}
              />
              <Route
                path="/signup"
                render={() => (
                  <Signup
                    onSignupSuccess={this.onSignupSuccess}
                    onSignupFailed={this.onSignupFailed}
                  />
                )}
              />
            </Switch>
          </div>
        </Content>
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: getIsAuthenticated(state),
    user: getUser(state)
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    actions
  )(App)
);
