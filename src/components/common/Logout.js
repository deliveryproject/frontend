import React, { Component } from "react";
import { Redirect } from "react-router-dom";

class Logout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navigate: false
    };
  }

  componentDidMount() {
    this.props.handleLogout();
    setTimeout(() => this.setState({ navigate: true }), 2000);
  }

  render() {
    if (this.state.navigate) {
      console.log("this.state.navigate", this.state.navigate);
      return <Redirect to="/" />;
    }

    return <p>You're successfully logouted. Redirecting...</p>;
  }
}

export default Logout;
