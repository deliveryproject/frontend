import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import "./AppHeader.css";
import { Layout, Menu, Dropdown, Icon } from "antd";
const Header = Layout.Header;

class AppHeader extends Component {
  render() {
    let menuItems;
    const { isAuthenticated, handleLogout } = this.props;
    if (isAuthenticated) {
      menuItems = [
        <Menu.Item key="logout">
          <Link to="/logout">Logout</Link>
        </Menu.Item>
      ];
    } else {
      menuItems = [
        <Menu.Item key="/login">
          <Link to="/login">Login</Link>
        </Menu.Item>,
        <Menu.Item key="/signup">
          <Link to="/signup">Signup</Link>
        </Menu.Item>
      ];
    }
    return (
      <Header className="app-header">
        <div className="container">
          <div className="app-title">
            <Link to="/">Delivery Project</Link>
          </div>
          <Menu
            className="app-menu"
            mode="horizontal"
            selectedKeys={[this.props.location.pathname]}
            style={{ lineHeight: "64px" }}
          >
            {menuItems}
          </Menu>
        </div>
      </Header>
    );
  }
}

export default withRouter(AppHeader);
