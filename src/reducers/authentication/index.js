import { combineReducers } from "redux";
import _ from "lodash";

export const isAuthenticated = (state = false, action) => {
  switch (action.type) {
    case "LOGIN_SUCCESS":
      return true;
    case "LOGIN_FAILURE":
      return state;
    case "LOGOUT_SUCCESS":
      return false;
    default:
      return _.isEmpty(localStorage.getItem("accessToken")) ? state : true;
  }
};

export const authenticationError = (state = {}, action) => {
  switch (action.type) {
    case "LOGIN_SUCCESS":
    case "LOGOUT_SUCCESS":
      return {};
    case "LOGIN_FAILURE":
    case "LOGOUT_FAILURE":
      return {
        errorCode: action.response.status,
        errorMessage: action.response.message
      };
    default:
      return state;
  }
};

export const user = (state = {}, action) => {
  switch (action.type) {
    case "LOGIN_SUCCESS":
      localStorage.setItem("accessToken", action.response.accessToken);
      return { ...action.response };
    case "LOGOUT_SUCCESS":
      return {};
    case "LOGIN_FAILURE":
    case "LOGOUT_FAILURE":
    default:
      return state;
  }
};

const actualUser = combineReducers({
  isAuthenticated,
  authenticationError,
  user
});

export default actualUser;

export const getIsAuthenticated = state => state.actualUser.isAuthenticated;
export const getUser = state => state.actualUser.user;
export const getAuthenticationError = state =>
  state.actualUser.authenticationError;
