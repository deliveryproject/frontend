export const byId = (state = {}, action) => {
  switch (action.type) {
    case "FETCH_ORDERS_SUCCESS":
      if (action.response) {
        console.log("response", action.response);
        return { ...state, ...action.response.entities.orders };
      }
    default:
      return state;
  }
};

export const isFetching = (state = false, action) => {
  switch (action.type) {
    case "FETCH_ORDERS_REQUEST":
      return true;
    case "FETCH_ORDERS_SUCCESS":
    case "FETCH_ORDERS_FAILURE":
      return false;
    default:
      return state;
  }
};

export const errorMessage = (state = null, action) => {
  switch (action.type) {
    case "FETCH_ORDERS_FAILURE":
      return action.message;
    case "LOGIN_SUCCESS":
    case "LOGOUT_SUCCESS":
      return null;
    default:
      return state;
  }
};
