import { schema } from "normalizr";

const order = new schema.Entity("orders");
export const orderSchema = new schema.Array(order);
