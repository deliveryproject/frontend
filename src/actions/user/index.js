import * as api from "../../api";
import { getIsAuthenticated, getUser } from "../../reducers/authentication";

export const getAuthenticatedUser = loginRequestParams => (
  dispatch,
  getState
) => {
  if (getIsAuthenticated(getState())) {
    return Promise.resolve;
  }

  return api
    .login(loginRequestParams)
    .then(response => {
      dispatch({
        type: "LOGIN_SUCCESS",
        response
      });
    })
    .catch(error => {
      if (error.status === 401) {
        dispatch({
          type: "LOGIN_FAILURE",
          response: error,
          message: error.message
        });
      } else {
        dispatch({
          type: "LOGIN_FAILURE",
          response: error,
          message: `Something went wrong. Error Code=${error.status}`
        });
      }
    });
};

export const signUpNewUser = signUpRequestParams => dispatch => {
  return api
    .signUp(signUpRequestParams)
    .then(response => {
      dispatch({
        type: "SIGNUP_SUCCESS",
        response
      });
    })
    .catch(error => {
      dispatch({
        type: "SIGNUP_FAILURE",
        response: error,
        message: error.message
      });
    });
};

export const handleLogout = () => dispatch => {
  console.log("handleLogout");
  localStorage.removeItem("accessToken");
  dispatch({ type: "LOGOUT_SUCCESS" });
};
