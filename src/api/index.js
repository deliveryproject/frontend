import { resolve } from "url";
import _ from "lodash";

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

const doRequest = async (url, method, requestParams) => {
  let headers = { "Content-Type": "application/json" };
  const accessToken = localStorage.getItem("accessToken");
  if (!_.isEmpty(accessToken)) {
    const authorizationHeader = { Authorization: "Bearer " + accessToken };
    headers = { ...headers, ...authorizationHeader };
  }
  const requestOptions = {
    method,
    headers,
    body: JSON.stringify(requestParams)
  };
  console.log("requestOptions", requestOptions);

  await delay(3000);
  const baseUrl = 'https://vadikproject.de:8443';
  const response = await fetch(baseUrl + url, requestOptions);
  const json = await response.json();
  return response.ok ? json : Promise.reject(json);
};

export const fetchOrders = async () => {
  const url = "/api/orders";

  return doRequest(url, "GET");
};

export const login = async loginRequestParams => {
  const url = "/api/auth/login";

  return doRequest(url, "POST", loginRequestParams);
};

export const signUp = async signUpRequestParams => {
  const url = "/api/auth/signup";

  return doRequest(url, "POST", signUpRequestParams);
};
